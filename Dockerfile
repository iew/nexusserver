# Nexus Server

FROM centos:7
MAINTAINER Greg Donarum <gdonarum@omi.com>

# dependencies
RUN yum -y update
# java
RUN yum -y install java-1.8.0-openjdk*
ENV JAVA_HOME /usr/lib/jvm/java-1.8.0-openjdk
# wget
RUN yum -y install wget

# vars
ENV nexus_version 2.13.0-01
ENV nexus_dir /opt/nexus
ENV nexus_user nexus
ENV work_dir /opt/sonatype-work

# install
RUN useradd -ms /bin/bash ${nexus_user}
RUN wget https://download.sonatype.com/nexus/oss/nexus-${nexus_version}-bundle.tar.gz
RUN tar xvfz nexus-${nexus_version}-bundle.tar.gz
RUN mv nexus-${nexus_version} ${nexus_dir}
#COPY org.sonatype.nexus.cfg ${nexus_dir}/etc
WORKDIR ${nexus_dir}
RUN chown -R ${nexus_user}:${nexus_user} ${nexus_dir}
RUN mkdir ${work_dir}
RUN chown -R ${nexus_user}:${nexus_user} ${work_dir}

# ports
EXPOSE 8081

# startup
CMD su -m $nexus_user -c "$nexus_dir/bin/nexus console"