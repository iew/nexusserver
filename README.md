# README #

This project holds the configuration settings for the IEW Nexus Server

https://books.sonatype.com/nexus-book/3.0/reference/install.html#installation-archive

### Running server with Docker ###

* docker build -t iew-nexus .
* docker run -i -p 8081:8081 iew-nexus

### Misc. ###
* "Couldn't connect to Docker daemon" on Mac and Cygwin --> eval "$(docker-machine env default)"

